import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import {Link, NavLink} from 'react-router-dom'
import {useState, useContext} from 'react'
import UserContext from '../UserContext'

function AppNavbar() {
  return (
    <Navbar className='navbar' bg="light" expand="lg">
    
      <Container fluid>
      <img className = "nav-logo" src="/ecommercewhite.png" alt="company logo" />
        <Navbar.Brand style={{marginLeft:'15%'}} href="#"></Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: '200px'}}
            navbarScroll
          >
            <Nav.Link as={NavLink} to="/products">Home</Nav.Link>
            <Nav.Link href="#action2">Link</Nav.Link>
            <NavDropdown title="Link" id="navbarScrollingDropdown">
              <NavDropdown.Item href="#action3">Action</NavDropdown.Item>
              <NavDropdown.Item href="#action4">
                Another action
              </NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action5">
                Something else here
              </NavDropdown.Item>
            </NavDropdown>
            <Nav.Link href="#" disabled>
              Link
            </Nav.Link>
          </Nav>
          <Form className="d-flex" style={{marginRight:'12%'}}>
            <Link to="/cart" style={{ textDecoration: 'none', color: 'black' }}>
                <img style={{width:'30px', height: '25px', margin: '10px'}} src="/cart.png" alt="cart" />
            </Link>
            <NavLink to='/login'>
                <button className='btn-login-nav'>Login</button>
            </NavLink>
            
          </Form>
        </Navbar.Collapse>
        
      </Container>
    </Navbar>
  );
}

export default AppNavbar;
 