import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function ProductView() {

	// Gets the courseId from the URL of the route that this component is connected to.'/course/:coursId'
	const {productId} = useParams()

	const {user} = useContext(UserContext)

	const navigate = useNavigate()
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1)

	

	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(response => response.json())
		.then(result => {
            
			setName(result[0].name)
			setDescription(result[0].description)
			setPrice(result[0].price)
			
		})
	},[productId])
	// console.log(name)
	const decrementQuantity = () => {
		if(quantity > 1){
			setQuantity(quantity - 1)
		}
	}

	// console.log(productId)
	const addToCart = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/addtocart/${quantity}`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			
			if(result.status === "success"){
				
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: result.message
				})
			} else {
				Swal.fire({
					title: 'Error',
					icon: 'error',
					text: 'Something went wrong.'
				})
			}
		})
	}

	return(
		<>
		{
			(name === "" || name === "undefined") ?
			<></>	
		:
			<Container className="mt-5x" style={{marginTop:'20 rem' , height: '100%'}}>
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card className='product-view-card' style={{width: '60%',marginTop:'100px', marginRight:'auto', marginLeft:'auto', boxShadow: '0px 8px 8px 2px rgba(181, 175, 175, 0.829)', border:'0'}}>
					<Card.Img variant="top" height='180' src="/image-cap.svg" />
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle style={{color: 'red', fontSize:'20px'}}>₱{price}</Card.Subtitle>
							<br/>
							
							{
								(localStorage.getItem('token')) ?
									<>
										<Card.Subtitle style={{fontSize:'20px'}}>Quantity: </Card.Subtitle>
										<Card.Text>
										<button className='cart-btn-ctr' onClick={decrementQuantity}>-</button>
											<input style={{fontWeight: 'bold'}} className='cart-input-quantity' type="number" value={quantity} onChange={(e) => setQuantity(e.target.value)} />
										<button className='cart-btn-ctr' onClick={() => setQuantity(quantity+1)}>+</button>
										</Card.Text>
										<Button onClick={addToCart} className='my-2'>Add to cart</Button>
									</>
								:
									<div style={{color: 'red'}}>*Please Login or Signup to buy this item*</div>
							}
							
							
						</Card.Body>		
					</Card>
				</Col>
			</Row>
			</Container>
		}
		</>
	)
}