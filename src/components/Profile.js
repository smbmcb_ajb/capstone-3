import {useState, useEffect,useContext} from 'react'
import {Form, Button, FormControl} from 'react-bootstrap';
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import {Link, NavLink} from 'react-router-dom'
import Nav from 'react-bootstrap/Nav'
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';

import {Container} from 'react-bootstrap';
import Image from 'react-bootstrap/Image'
import React from 'react';
import Modal from 'react-bootstrap/Modal';



export default function Profile(){
  const {unsetUser, setUser} = useContext(UserContext)
    const [show, setShow] = useState(false);
    const [oldPassword, setOldPassword] = useState('')
    const [newPassword, setNewPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [passwordIcon, setPAsswordIcon] = useState('/close-eye.png')
    const [type, setType] = useState('password')

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  var d = new Date(localStorage.getItem('joinDate'))

  const changePassword = () => {
    fetch(`${process.env.REACT_APP_API_URL}/users/change-password`, {
      method: 'PATCH',
      headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        oldPassword:oldPassword,
        newPassword: newPassword,
        confirmPassword: confirmPassword
        
    })
 
  })
  .then(response => response.json())
  .then(result => {
      if(result.status === "success"){
        
          Swal.fire({
              title: "Success",
              icon: 'success',
              text: result.message
          })
          setOldPassword('')
          setNewPassword('')
          setConfirmPassword('')
          handleClose()
          unsetUser()
          return(
            <Navigate to='/login'/>
          )
      } else {
          Swal.fire({
              title: 'Error',
              icon: 'error',
              text: result.message
          })
      }
  })
  }

  const showPassword = () => {
    if(type === 'password') {
        setType('text')
        setPAsswordIcon('eye.png')  
    } else {
        setType('password')
        setPAsswordIcon('close-eye.png')  
    }
        
}



  return (
    <div style={{fontSize:'25px'}}>
    <img src="/profile-pic.png" alt="profile logo" />
      {
        (localStorage.getItem('admin') === "true") ?
          <span>Admin</span>
        :
          <></>
      }
    <br/>
    Name: {localStorage.getItem('name')}
    <br/>
    Email: {localStorage.getItem('email')}
    <br/>
    Registered on: {d.toLocaleString()}
    <br/>
    <Button className='mb-3 mt-3' variant="primary" onClick={handleShow}>
        Change Password
    </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Change Password</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Form>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Old Password</Form.Label>
            <Form.Control type="text" placeholder="Password" value={oldPassword} onChange={(e) => setOldPassword(e.target.value)}/> 
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>New Password</Form.Label>
            <Form.Control type={type} placeholder="Password" value={newPassword} onChange={(e) => setNewPassword(e.target.value)}/>
            <FormControl.Feedback>
              <span style={{ top: '5px' }}>
                <div onClick={showPassword}><img style={{width:'20px', height: '25px', marginLeft: '-20px'}} src={passwordIcon} alt="eye" /></div>
              </span>
            </FormControl.Feedback>
            
          </Form.Group>
          <FormControl.Feedback>
              <span style={{ top: '5px' }}>
                <div onClick={showPassword}><img style={{width:'20px', height: '25px', marginLeft: '-20px'}} src={passwordIcon} alt="eye" /></div>
              </span>
            </FormControl.Feedback>
            
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Confirm Password</Form.Label>
            <Form.Control type={type} placeholder="Password" value={confirmPassword} onChange={(e) => setConfirmPassword(e.target.value)}/>
          </Form.Group>
          
          
        </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={changePassword}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
     
    </div>

  );
}
