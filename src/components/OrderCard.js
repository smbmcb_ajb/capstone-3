import {useState, useEffect} from 'react';
import { Form, Button } from "react-bootstrap"
import Card from 'react-bootstrap/Card';
import PropTypes from 'prop-types';
import {Link, NavLink} from 'react-router-dom'
import Accordion from 'react-bootstrap/Accordion';
import Table from 'react-bootstrap/Table';


export default function OrderCard({order}) {
 

  
  var d = new Date(order.createdOn)
    // order.order.map((data) => {
    //     console.log(data.name)
    // })
  // Destructuring the props
//   const {productName, quantity, price, subTotal, status, orderDate, rider, totalAmount,_id} = order
//   console.log(order.order)
  // Using the state
  // Initialize a 'count' state with a value of zero (0)
  const [count, setCount] = useState(0)
  const [slot, setSlot] = useState(15)
  const [isOpen, setIsOpen] = useState(true)

 
let breakdownItems = (datas) => {
   return datas.map(data => {
        return data.name
    })
}
//   console.log(breakdownItems(order.order))

  return (

    <>
      
    
        <Accordion defaultActiveKey="0" className='my-1'>
          
          <Accordion.Item eventKey="1"> Ordered On: {d.toLocaleString()} | Order id: {order._id}
          {order.order.map((object,index) => (
            <Accordion.Header key={index}>{object.name} | {order.status} | <span style={{color:'blue'}}>Click for details</span></Accordion.Header>
            ))}
            <Accordion.Body>
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Sub-Total</th>
                  </tr>
                </thead>
                <tbody>
                {order.order.map((object,index) => (
                      <tr key={index}>
                      <td>{object.name}</td>
                      <td>{object.quantity}</td>
                      <td>{object.price}</td>
                      <td>{object.subTotal}</td>
                    </tr>
                    ))}
                  <tr>
                      <td colSpan={4} style={{color: 'red'}}>Total Order: {order.totalAmount}</td>
                  </tr>
                  
                </tbody>
              </Table>
            </Accordion.Body>
          </Accordion.Item>
        </Accordion>
      </>
  );
}



// Prop Types can be used to validate the data coming from the props. You can define each property of the prop and assign specific validation for each of them
// ProductCard.propTypes = {
//   course: PropTypes.shape({
//     name:PropTypes.string.isRequired,
//     description: PropTypes.string.isRequired,
//     price: PropTypes.number.isRequired
//   })
// }