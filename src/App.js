import './App.css';
import {useState, useContext, useEffect} from 'react'
import {UserProvider} from './UserContext'
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router,Route, Routes} from 'react-router-dom'
import Login from './pages/Login';
import Products from './pages/Products'
import 'font-awesome/css/font-awesome.min.css';
import UserContext from './UserContext'
import AppNavbar from './components/AppNavbar'
import Navbar from './components/Navbar'
import Register from './pages/Register'
import Logout from './pages/Logout'
import Account from './pages/Account'
import Order from './pages/Order'
import ProductView from './components/ProductView'
import Home from './pages/Home'
import AdminProducts from './adminPages/Products'
import Cart from './pages/Cart'
import ErrorPage from './pages/ErrorPage'
import AdminOrders from './adminPages/Orders'
import Users from './adminPages/Users'
import VerificationEmail from './pages/VerificationEmail'

function App() {
 
  


  const [user, setUser] = useState({
    id:null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  const [show, setShow] = useState('none')
  
  const chatCollapse = () => {
    document.querySelector("#textarea").focus()
    if(show === 'none'){
      setShow('flex')
      
      
    } else {
      setShow('none')
    }
    document.getElementById('incoming-message').scrollIntoView(false)
  }
const [textMessage, setTextMessage] = useState([])
const [messages, setMessages] = useState([]);

  const sendMessage = ()=> {
  
    fetch(`${process.env.REACT_APP_API_URL}/messages/send-message/6342d9f440dd99b6bc1557fa`, {
      method: 'POST',
			headers:{
        'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
      body: JSON.stringify({
        message: textMessage
      })
		})
		.then(response => response.json())
		.then(()=> {
      setTextMessage('')
      setTimeout(()=>{
        document.getElementById('incoming-message').scrollIntoView(false)
        
      },1000)
      document.getElementById('textarea').innerText = ""
    })
  }
useEffect(()=> {
  fetch(`${process.env.REACT_APP_API_URL}/messages/get-messages/6342d9f440dd99b6bc1557fa`, {
    method: 'GET',
    headers:{
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
  })
  .then(response => response.json())
  .then(result => {
    // console.log(result)
    if(!result.auth){
      // console.log(result)
      setMessages(result);
      // console.log(messages)
    }
    
  })
})

if(messages !== undefined){
  // console.log(messages)
}
// console.log(messages)

  
  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
      <Navbar />
        {/* <AppNavbar /> */}
        <Container  className='app-container' fluid style={{margin:"0px", width:"100%", height: '100%', overflow:'scroll'}}>
          {
            (localStorage.getItem('admin') === "false" && localStorage.getItem('csr') !== "true") ?
                <img className='chat-btn' onClick={chatCollapse} src='/csr.png'></img>
              :
                <></>
          }
          

          {
            (localStorage.getItem('admin') === "false" && localStorage.getItem('csr') !== "true") ?
              <div className='chat-popup' style={{display:show}}>
              <div style={{height: '50px', borderBottom: '1px solid gray'}}>
                <h6 style={{marginTop: '10px', marginLeft:'5px'}}>Sending message to Customer support:</h6> 
              </div>
              <div className='chat-area'>
                <div className='incoming-message' id='incoming-message' style={{display:'block' ,}}>
                  
                  {/* <div style={{float: 'left'}} className='msg'>Hi, how can I help you?</div>
                  <div style={{float: 'right'}} className='msg'>Reset password please</div>
                  <div style={{float: 'left'}} className='msg'>Hi, how can I help you?</div>
                  <div style={{float: 'right'}} className='msg'>Reset password please</div> */}
                  { (messages !== undefined) ?
                      messages.map((m, index)=> (
                        
                        <div key={index} style={{display: 'flex', flexDirection: 'column'}}>
                          <div className='date-line-through' style={{  width: '100%', height:'25px', color: 'gray', fontSize:'12px'}}><center>{(new Date(m.sentOn)).toLocaleString()}</center></div>
                          <div style={{ float: 'right', width: '200px', alignSelf:'flex-end'}}>
                            {
                              (m.from) ?
                                <div key={index} style={{padding:'3px',border: '1px solid gray', borderRadius:'10px 10px 1px 10px' ,float: 'right', minWidth: '50px',maxWidth:'200px',maxHeight:'80%', minHeight: '25px', textAlign:'center', marginRight:'10px',overflowWrap:'break-word'}} className='msg'>{m.from}</div>
                              :
                              <></>
                            }
                            
                          </div>
                          <div style={{ float: 'left', width: '200px'}}>
                            {
                              (m.to) ?
                                <div  style={{padding:'3px', border: '1px solid gray', borderRadius:'10px 10px 10px 1px' ,minWidth: '50px', maxWidth:'200px', maxHeight:'100%', minHeight: '25px', float: 'left', marginLeft:'10px', overflowWrap:'break-word'}} className='msg'>{m.to}</div>
                              :
                              <></>
                            }
                          </div>
                        </div>
                          
                        
                          // (m) ?
                          //   <div key={index} style={{float: 'right'}} className='msg'>{m}</div>
                          // :
                          //   <div key={index} style={{float: 'left'}} className='msg'>{m}</div>
                      
                      ))
                    :
                      <></>
                  }
                  {/* { (messages) ?
                      messages.from.map((m)=> (
                        <div key={m._id} style={{float: 'left'}} className='msg'>{m.messages}</div>
                      ))
                    :
                      <></>
                  } */}
                  
                </div>
              </div>

              <div className='input-area'>
                <span id='textarea' className='textarea' role='textbox' contentEditable onSelect={(e) => setTextMessage(e.target.innerText) } ></span>
                <button onClick={sendMessage} value={textMessage} className='send-btn'>Send</button>
              </div>
              </div>
            :
              <></>
          }
          
          <Routes>
          <Route path="/" element={<Home/>}/>
            <Route path="/login" element={<Login/>}/>
            {
               (localStorage.getItem('admin') === "true") ?
                  <>
                      <Route path="/account" element={<Account/>}/>
                      <Route path="/admin/products" element={<AdminProducts/>}/>
                      <Route path="/admin/orders" element={<AdminOrders/>}/>
                      <Route path="/admin/users" element={<Users/>}/>
                  </>
                :
                  <>
                  </>
            }
            {
              (localStorage.getItem('admin') === "false") ?
                <>
                    
                    <Route path="/account" element={<Account/>}/>
                    <Route path="/cart" element={<Cart/>}/>
                </>
              :
                <></>
            }
            <Route path="/products" element={<Products/>}/>
            <Route path="/products/:productId" element={<ProductView/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="*" element={<ErrorPage/>}/>
            <Route path="users/email/:email/:passphrase/verification/success" element={<VerificationEmail/>}/>
            
            
          </Routes>
        </Container>  
      </Router>
    </UserProvider>
      
    </>
  );
}

export default App;
