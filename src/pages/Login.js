import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import {Link, NavLink} from 'react-router-dom'
import Nav from 'react-bootstrap/Nav'
import {useNavigate, Navigate} from 'react-router-dom'

export default function Login(){
    const navigate = useNavigate()
    const {user, setUser} = useContext(UserContext)
    const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
    const [error, setError] = useState('')
    let [display, setDisplay] = useState('')
    const [type, setType] = useState('password')
    const [passwordIcon, setPAsswordIcon] = useState('/close-eye.png')
    const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/

    useEffect(()=> {
		if(email !== "" || password !== ""){
            setDisplay("none")
        }
	},[email, password])

    const retrieveUser = (token) => {
        // console.log(token)
		fetch("https://capstone-2-bautista-1dbm.onrender.com/users/details", {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(result => {
			// console.log(result)
            localStorage.setItem('admin', result.isAdmin)
            localStorage.setItem('name', result.firstName + " " + result.lastName)
            localStorage.setItem('firstName', result.firstName)
            localStorage.setItem('joinDate', result.registeredOn)
            localStorage.setItem('email', result.email)
            localStorage.setItem('csr', result.isCustomerService)
            localStorage.setItem('id', result._id)
            // console.log(localStorage.getItem('csr'))
			// Store the user details retrieved from the token into the global user state
			setUser({
				id: result._id,
				isAdmin: result.isAdmin
			})
            // console.log(user)
		})
	}

    const sign_in = (e) => {
        e.preventDefault()
        if(!email.match(mailformat)){
                setDisplay("flex")
                setError("Invalid email format!")
        } else
        if(email === "" || email === null || password === "" || password === null){
            setError("Invalid credentials!")
            setDisplay("flex")
        } else {
            fetch("https://capstone-2-bautista-1dbm.onrender.com/users/login", {
                method: 'POST',
                headers: {
                    'Content-Type':'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            })
            .then(response => response.json())
            .then(result => {
                if(typeof result.accessToken !== "undefined"){
                    // console.log(result.accessToken)
                    localStorage.setItem('token', result.accessToken)
                    retrieveUser(result.accessToken)

                    Swal.fire({
                        title: 'Login Successful!',
                        icon: 'success',
                        text: result.message
                    })
                    if(localStorage.getItem('admin') === "true"){
                        // console.log("admin")
                        return(
                            <Navigate to='/admin/products'/>
                        )
                    } else {
                        // console.log("not admin")
                        return(
                            <Navigate to='/products'/>
                        )
                    }
                } else {
                    setError(result.message)
                    setDisplay("flex")
                }
            })
            // console.log(localStorage.getItem('token'))
        }
    }
    
    const showPassword = () => {
        if(type === 'password') {
            setType('text')
            setPAsswordIcon('eye.png')  
        } else {
            setType('password')
            setPAsswordIcon('close-eye.png')  
        }
            
    }

    if(localStorage.getItem('token')){
        if(localStorage.getItem('admin') === "true"){
            // console.log("admin")
            return(
                <Navigate to='/admin/products'/>
            )
        } else {
            // console.log("not admin")
            return(
                <Navigate to='/products'/>
            )
        }
        
    }

    return(
        <div className='content' id="body">
            <div className="logo">
                <center>
                    {/* <h1>Online-Tindahan</h1> */}
                    <img className = "logo_login mt-5" src="/ontindahan3.png" alt="company logo" />
                    
                </center>
            </div>
            {/* <div className="login-title"><h1>Online Tindahan</h1></div> */}
            <div className="log-in">
                <center><h2>Account Log-in</h2></center>
                
                
                <form action="#" method="POST">
                    <div className="username-field">
                        <label htmlFor="email" className="label-field">Email</label><br />
                        <input id="email" type="email" name="email" onChange={event => setEmail(event.target.value)} value={email} className="input-field"  required />
                    </div>
                    <div className="username-field">
                        <label htmlFor="password" className="label-field">Password</label><br />
                        <input id="password" type={type} name="password" onChange={event => setPassword(event.target.value)} value={password} className="input-field" />
                        <div onClick={showPassword}><img style={{width:'20px', height: '25px', marginLeft: '-20px'}} src={passwordIcon} alt="eye" /></div>
                        
                    </div>
                    
                    
                    <div className="username-field">
                        <label id="login_error" className="login_error" style={{display:display}}>{error}</label><br />
                    </div>

                    <div className="username-field">
                        <button id="sign_in" className="btn-sign-in" type="submit" onClick={sign_in} name="sign-in">Sign-in</button>
                    </div>
                    <center><p>No account yet? Register <Nav.Link as={NavLink} to="/register" style={{color: 'blue', fontWeight:"bold"}}>Here</Nav.Link></p></center>
                </form>
            </div>
        </div>
    )
}
