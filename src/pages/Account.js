

import {useState, useEffect,useContext} from 'react'
import {Form, Button} from 'react-bootstrap';
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import {Link, NavLink} from 'react-router-dom'
import Nav from 'react-bootstrap/Nav'
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import Login from './Login'
import {Container} from 'react-bootstrap';
import Profile from '../components/Profile'
import Order from './Order'

// import Sonnet from '../../components/Sonnet';



export default function Account(){
  // console.log(localStorage.getItem('admin'))
    

  return (
    <Container style={{ width:'90%', maxHeight:'75vh', boxShadow: '0px 2px 10px 0px gray', position:"fixed", top: '150px', left: '5%', borderRadius:'10px', overflowY: 'scroll'}}>
    
      
      {
          (localStorage.getItem('admin') === "true") ?
            <Tabs
              defaultActiveKey="account"
              id="justify-tab-example"
              className="mb-3"
              justify
              style={{
                position:"fixed",
                top: '100px',
                left: '25%',
                fontWeight: 'bold',
                width: '50%',
                
              }}
            >
              <Tab className='nav-tab' eventKey="account" title="My Account">
                <Profile />
              </Tab>
            </Tabs>
          :
            <Tabs
              defaultActiveKey={localStorage.getItem('key')}
              id="justify-tab-example"
              className="mb-3"
              justify
              style={{
                position:"fixed",
                top: '100px',
                left: '25%',
                fontWeight: 'bold',
                width: '50%',
                
            }}
            >
            <Tab className='nav-tab' eventKey="account" title="My Account">
              <Profile />
            </Tab>
            <Tab eventKey="orders" title="My Orders">
              <Order />
            </Tab>
            <Tab eventKey="longer-tab" title="My Vouchers">
                <center><h1>Coming soon</h1></center>
            </Tab>
          </Tabs>
              
      }
      
      
    
    </Container>
 
  );
}
