import {Link} from 'react-router-dom'

export default function ErrorPage(){
	return (
		<div style={{
            marginTop: '15%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column'
        }}>
			<h1 style={{fontSize:'100px',textShadow:'3px 3px 5px gray', fontWeight:'bolder'}}>404</h1>
			<h3 style={{width:'70%',textAlign:'center'}}>we're fairly sure that page used to be here, but seems to have gone missing.We do apologize on it's behalf.</h3>
			<Link style={{textDecoration:'none'}} to="/"><center><button style={{marginTop:'15px'}} className='btn-login-nav mx-auto'>Home</button></center></Link>
		</div>
		
	)
}