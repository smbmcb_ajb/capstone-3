import {useState, useEffect} from 'react';
import { Form, Button, Container } from "react-bootstrap"
import Card from 'react-bootstrap/Card';
import PropTypes from 'prop-types';
import {Link, NavLink} from 'react-router-dom'
import Accordion from 'react-bootstrap/Accordion';
import Table from 'react-bootstrap/Table';
import Swal from 'sweetalert2';


export default function OrderCard({order}) {
 

  
  var d = new Date(order.createdOn)
    // order.order.map((data) => {
    //     console.log(data.name)
    // })
  // Destructuring the props
//   const {productName, quantity, price, subTotal, status, orderDate, rider, totalAmount,_id} = order
//   console.log(order.order)
  // Using the state
  // Initialize a 'count' state with a value of zero (0)
  const [count, setCount] = useState(0)
  const [slot, setSlot] = useState(15)
  const [isOpen, setIsOpen] = useState(true)
  const [status, setStatus] = useState('')

 
let breakdownItems = (datas) => {
   return datas.map(data => {
        return data.name
    })
}

const updateStatus = () => {
  // console.log(status)
  console.log(order._id)
  if(status !== ""){
    fetch(`${process.env.REACT_APP_API_URL}/orders/admin/update-status`, {
      method: 'PATCH',
      headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
          status: status,
          id:order._id
      })
  })
  .then(response => response.json())
  .then(result => {
      if(result.status === "success"){
          
          Swal.fire({
              title: 'Success',
              icon: 'success',
              text: result.message
          })
      } else {
        Swal.fire({
          title: 'Error',
          icon: 'error',
          text: result.message
      })
      }
  })
  }

}
//   console.log(breakdownItems(order.order))

  return (

    <>
      
    
        <Accordion defaultActiveKey="0" className='my-1'>
          
          <Accordion.Item eventKey="1">Ordered on: {d.toLocaleString()} | Order id: {order._id}
          {order.order.map((object,index) => (
            <Accordion.Header key={index}>{object.name} | {order.status} | <span style={{color:'blue'}}>Click for details</span></Accordion.Header>
            ))}
            <Accordion.Body>
            <div style={{display:'flex',flexDirection:'row', width:'100%'}}>
              Purchased by: {order.name} 
            <Form.Select onChange={(e) => setStatus(e.target.value)} style={{width:'50%', marginLeft:'10px'}} size="sm">
              <option value="">Change status</option>
              <option value='for-delivery'>For Delivery</option>
              <option value='completed'>Completed</option>
              <option value='cancelled'>Cancelled</option>
            </Form.Select>
            <Button onClick={updateStatus} style={{width:'25%'}} variant="success">Update</Button>
            </div>
             
            
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Sub-Total</th>
                  </tr>
                </thead>
                <tbody>
                {order.order.map((object,index) => (
                      <tr key={index}>
                      <td>{object.name}</td>
                      <td>{object.quantity}</td>
                      <td>{object.price}</td>
                      <td>{object.subTotal}</td>
                    </tr>
                    ))}
                  <tr>
                      <td colSpan={4} style={{color: 'blue'}}>Total Order: {order.totalAmount}</td>
                  </tr>
                  <tr>
                      <td colSpan={4}>Date Purchased: {d.toLocaleString()}</td>
                  </tr>
                </tbody>
              </Table>
            </Accordion.Body>
          </Accordion.Item>
        </Accordion>
      </>
  );
}



// Prop Types can be used to validate the data coming from the props. You can define each property of the prop and assign specific validation for each of them
// ProductCard.propTypes = {
//   course: PropTypes.shape({
//     name:PropTypes.string.isRequired,
//     description: PropTypes.string.isRequired,
//     price: PropTypes.number.isRequired
//   })
// }